import yaml
import os

# dict = yaml.SafeLoader(open('stages.yml'))
def yaml_loader(filepath):
    with open(filepath, "r") as file_descriptor:
        data = yaml.safe_load(file_descriptor)
    return data

#set titles for users
KEYS = ['pm','pmm','cm','backend_engineering_manager', 'frontend_engineering_manager', 'support', 'sets', 'pdm','ux', 'uxr','tech_writer', 'tw_backup', 'appsec_engineer', 'product_sponsor']
#create list for final group membership
stored_groups = []


def gets_group_membership(data):
    # for each stage
    for stage in data:
        # for each group in stage
        for group in data[stage]['groups']:
            # Create your list of people
            group_object = data[stage]['groups'][group]
            group_members = []
            pairs = {'eng' + '-' + 'dev' + '-' + stage + '-' + group : group_members}
            # for all keys in group
            for key,value in group_object.items():
                # if valid key
                if key in KEYS:
                    # add to list
                    group_members.append(value)
            stored_groups.append(pairs)
    return stored_groups        

if __name__ == "__main__":
    rawFile = yaml_loader("stages.yml")
    data = rawFile['stages']

    final_result = gets_group_membership(data)
    print(*final_result, sep='\n')
